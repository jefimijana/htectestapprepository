package com.htec.jefimijana.htectestapplication.activities;

import android.app.Instrumentation;
import android.support.test.rule.ActivityTestRule;
import android.view.View;

import com.htec.jefimijana.htectestapplication.R;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.junit.Assert.*;

/**
 * Main activity integration test
 */
public class MainActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> activityActivityTestRule = new ActivityTestRule<>(MainActivity.class);
    public MainActivity mainActivity;
    public Instrumentation.ActivityMonitor monitor = getInstrumentation().addMonitor(ItemDetailsActivity.class.getName(), null, false);

    @Before
    public void setUp() throws Exception {
        mainActivity = activityActivityTestRule.getActivity();
    }

    /**
     * Tests main recycler view on item click and item details activity launching
     */
    @Test
    public void testLaunch() {
        View view = mainActivity.findViewById(R.id.rvMainRecycler);
        assertNotNull(view);
    }

    @Test
    public void testLaunchItemDetailsActivity() {

        //Arrange
        assertNotNull(mainActivity.findViewById(R.id.rvMainRecycler));

        //Act
        onView(withId(R.id.rvMainRecycler)).perform(click());

        //Assert
        ItemDetailsActivity itemDetailsActivity = (ItemDetailsActivity) getInstrumentation().waitForMonitorWithTimeout(monitor, 2000);
        assertNotNull(itemDetailsActivity);
    }

    @After
    public void tearDown() throws Exception {
        mainActivity = null;
    }
}