package com.htec.jefimijana.htectestapplication;

import java.io.Serializable;

/**
 * Represents single item from json array received from web
 */
public class Data implements Serializable {

    /**
     * Image url
     */
    private String imageUrl;

    /**
     * Data item description
     */
    private String description;

    /**
     * Data item title
     */
    private String title;

    /**
     * Constructor
     *
     * @param imageUrl    data image url
     * @param description data item description
     * @param title       data item title
     */
    public Data(String imageUrl, String description, String title) {
        this.imageUrl = imageUrl;
        this.description = description;
        this.title = title;
    }

    public String getImageUrl() {

        //Change http to https
        //TODO: Better solution
        String imageUrl = this.imageUrl;
        String httpPart = imageUrl.substring(0, 4);
        String restOfUrl = imageUrl.substring(5, this.imageUrl.length() - 1);
        httpPart = httpPart + "s:";
        this.imageUrl =  httpPart + restOfUrl;
        return this.imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
