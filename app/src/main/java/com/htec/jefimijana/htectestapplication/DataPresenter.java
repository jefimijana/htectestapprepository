package com.htec.jefimijana.htectestapplication;

import java.util.List;

/**
 * Data presenter
 */
public class DataPresenter {

    /**
     * Data view interface
     */
    private DataView view;

    /**
     * Data service used for fetching data from the web
     */
    private DataService service;

    /**
     * Constructor
     *
     * @param view
     * @param service
     */
    public DataPresenter(DataView view, DataService service) {
        this.view = view;
        this.service = service;
    }

    /**
     * Requests data from the web
     */
    public void onDataRequested() {
        List<Data> items = service.getData();
        if (items == null) {
            view.onDataRequestFail();
        }
    }

    /**
     * Invoked when recycler view item is clicked
     *
     * @param item clicked item
     */
    public void onDataItemClicked(Data item) {
        view.onDataItemClicked(item);
    }
}
