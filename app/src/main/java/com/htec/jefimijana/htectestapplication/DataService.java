package com.htec.jefimijana.htectestapplication;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.htec.jefimijana.htectestapplication.utils.Constants;
import com.htec.jefimijana.htectestapplication.utils.NetworkHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Used to fetch data from the web
 */
public class DataService {

    /**
     * Current context
     */
    private Context context;

    /**
     * Reference to main adapter
     */
    private MainAdapter adapter;

    /**
     * Constructor
     *
     * @param context current context
     * @param adapter adapter for data items
     */
    public DataService(Context context, MainAdapter adapter) {
        this.context = context;
        this.adapter = adapter;
    }

    /**
     * Asynchronous data get from web storage
     */
    public List<Data> getData() {

        //List of data items
        final ArrayList<Data> items = new ArrayList<>();

        //Initialize a new JsonArrayRequest instance
        JsonArrayRequest jsObjRequest = new JsonArrayRequest(
                Request.Method.GET,
                Constants.DATA_SOURCE,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        //Loop through the array of items
                        for (int j = 0; j < response.length(); j++) {
                            try {

                                //Get json object
                                JSONObject itemJsonObject = response.getJSONObject(j);

                                //Get json object key values
                                String image = itemJsonObject.getString(Constants.DATA_IMAGE);
                                String description = itemJsonObject.getString(Constants.DATA_DESCRIPTION);
                                String title = itemJsonObject.getString(Constants.DATA_TITLE);

                                //Create item
                                Data item = new Data(image, description, title);

                                //Add item to list of items
                                items.add(item);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        //Items are added to the list

                        //Refresh adapter with received data
                        adapter.refresh(items);

                        return;
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError e) {
                        // Do something when error occurred
                    }
                }
        );

        //Perform network request by adding it to que
        NetworkHandler.getInstance(context).addToRequestQueue(jsObjRequest);

        return null;
    }
}
