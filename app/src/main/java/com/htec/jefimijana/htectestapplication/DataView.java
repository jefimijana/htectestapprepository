package com.htec.jefimijana.htectestapplication;

/**
 * Used to display fetched data
 */
public interface DataView {

    /**
     * Invoked when data request fails
     */
    void onDataRequestFail();

    /**
     * Invoked when item in list of items is clicked
     *
     * @param item
     */
    void onDataItemClicked(Data item);
}
