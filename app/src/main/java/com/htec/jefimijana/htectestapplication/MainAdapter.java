package com.htec.jefimijana.htectestapplication;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Used to hold and bind recycler view data
 */
public class MainAdapter extends RecyclerView.Adapter {

    /**
     * Context
     */
    private Context context;

    /**
     * List of data items
     */
    private List<Data> items = new ArrayList<>();

    /**
     * Main adapter listener
     */
    private MainAdapterListener listener;

    /**
     * Constructor
     *
     * @param context  current context
     * @param listener adapter listener
     */
    public MainAdapter(Context context, MainAdapterListener listener) {
        this.context = context;
        this.listener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        //Get layout inflater and inflate item layout
        LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View itemView = inflater.inflate(R.layout.recycler_view_item_layout, null);

        //Create view holder for single item
        final DataViewHolder dataItemViewHolder = new DataViewHolder(itemView);

        //Set on click listener
        dataItemViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClicked(items.get(dataItemViewHolder.getAdapterPosition()));
            }
        });

        return dataItemViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        //Bind data item title
        DataViewHolder itemViewHolder = (DataViewHolder) holder;
        itemViewHolder.tvItemTitle.setText(items.get(position).getTitle());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    /**
     * Refreshes adapter data list
     *
     * @param items list of data items
     */
    public void refresh(List<Data> items) {
        this.items.clear();
        this.items.addAll(items);
        notifyDataSetChanged();
    }

    /**
     * View holder used for single data item in list of items
     */
    public class DataViewHolder extends RecyclerView.ViewHolder {

        /**
         * Text view used for displaying data title
         */
        TextView tvItemTitle;

        /**
         * Constructor
         *
         * @param itemView inflated layout used for single data item
         */
        public DataViewHolder(View itemView) {
            super(itemView);

            //Get reference to title text view
            tvItemTitle = itemView.findViewById(R.id.tvMainRecyclerItemTitle);
        }
    }

    /**
     * Listener for events in main recycler adapter
     */
    public interface MainAdapterListener {

        /**
         * Invoked when adapter item clicked
         *
         * @param item clicked item
         */
        void onItemClicked(Data item);
    }
}
