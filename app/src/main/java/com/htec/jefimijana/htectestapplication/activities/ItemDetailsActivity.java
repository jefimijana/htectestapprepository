package com.htec.jefimijana.htectestapplication.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.htec.jefimijana.htectestapplication.Data;
import com.htec.jefimijana.htectestapplication.R;
import com.htec.jefimijana.htectestapplication.utils.Constants;
import com.htec.jefimijana.htectestapplication.utils.ImageLoader;

/**
 * Displays details about single data item
 */
public class ItemDetailsActivity extends AppCompatActivity {

    /**
     * Data item
     */
    private Data item;

    /**
     * Text view for item title
     */
    private TextView tvTitle;

    /**
     * Image view for item image
     */
    private ImageView ivItemImage;

    /**
     * Text view for item description
     */
    private TextView tvDescription;

    /**
     * Loads images form web
     */
    private ImageLoader imageLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_details);

        //Create image loader
        imageLoader = new ImageLoader(getApplicationContext());

        //Get view references
        tvTitle = findViewById(R.id.tvItemDetailsTitle);
        ivItemImage = findViewById(R.id.ivItemDetailsImage);
        tvDescription = findViewById(R.id.tvItemDetailsDescription);

        //Get item from intent
        Intent itemIntent = getIntent();
        Bundle itemBundle = itemIntent.getBundleExtra(Constants.DATA_BUNDLE);
        item = (Data) itemBundle.getSerializable(Constants.DATA);

        //Bind data
        tvTitle.setText(checkText(item.getTitle()));
        imageLoader.loadImage(ivItemImage, item.getImageUrl());
        tvDescription.setText(checkText(item.getDescription()));
    }

    /**
     * Checks item properties values when received from intent
     *
     * @param text
     * @return text
     */
    private String checkText(String text) {
        if (text != null && !text.isEmpty()) {
            return text;
        }
        return "";
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_right);
    }
}
