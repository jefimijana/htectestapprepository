package com.htec.jefimijana.htectestapplication.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.htec.jefimijana.htectestapplication.Data;
import com.htec.jefimijana.htectestapplication.DataPresenter;
import com.htec.jefimijana.htectestapplication.DataService;
import com.htec.jefimijana.htectestapplication.DataView;
import com.htec.jefimijana.htectestapplication.MainAdapter;
import com.htec.jefimijana.htectestapplication.R;
import com.htec.jefimijana.htectestapplication.utils.Constants;

/**
 * Main activity
 */
public class MainActivity extends AppCompatActivity implements DataView {

    /**
     * Data presenter
     */
    private DataPresenter dataPresenter;

    /**
     * Recycler view used for displaying list of items
     */
    private RecyclerView rvMain;

    /**
     * Custom adapter used for holding data of main recycler
     */
    private MainAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Get reference to recycler view
        rvMain = findViewById(R.id.rvMainRecycler);

        //Create and add layout manager for ui renderings
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rvMain.setLayoutManager(layoutManager);

        //Create and set main adapter
        adapter = new MainAdapter(this, new MainAdapter.MainAdapterListener() {
            @Override
            public void onItemClicked(Data clickedItem) {
                dataPresenter.onDataItemClicked(clickedItem);
            }
        });
        rvMain.setAdapter(adapter);

        //Initialize data presenter and request data
        dataPresenter = new DataPresenter(this, new DataService(this, adapter));
        dataPresenter.onDataRequested();
    }

    @Override
    public void onDataRequestFail() {
        //TODO: Proper async unit tests
    }

    @Override
    public void onDataItemClicked(Data item) {

        //Navigate to item details activity
        Intent itemDetailsIntent = new Intent(MainActivity.this, ItemDetailsActivity.class);
        Bundle itemDetailsBundle = new Bundle();
        itemDetailsBundle.putSerializable(Constants.DATA, item);
        itemDetailsIntent.putExtra(Constants.DATA_BUNDLE, itemDetailsBundle);
        startActivity(itemDetailsIntent);
        overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);
    }
}
