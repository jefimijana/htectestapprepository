package com.htec.jefimijana.htectestapplication.utils;

/**
 * String constants, used with key value pairs mostly
 */
public class Constants {

    public static final String DATA_SOURCE = "https://raw.githubusercontent.com/danieloskarsson/mobile-coding-exercise/master/items.json";
    public static final String DATA = "data";
    public static final String DATA_BUNDLE = "data_bundle";
    public static final String DATA_IMAGE = "image";
    public static final String DATA_DESCRIPTION = "description";
    public static final String DATA_TITLE = "title";
}
