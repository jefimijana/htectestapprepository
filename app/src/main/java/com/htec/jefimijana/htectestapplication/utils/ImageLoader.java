package com.htec.jefimijana.htectestapplication.utils;


import android.content.Context;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

/**
 * Uses Picasso to download images from specified url
 */
public class ImageLoader {

    /**
     * Context
     */
    private Context context;

    /**
     * Constructor
     *
     * @param context
     */
    public ImageLoader(Context context) {
        this.context = context;
    }

    /**
     * Loads image and sets it as background
     *
     * @param imageView
     * @param itemImageUrl
     */
    public void loadImage(ImageView imageView, String itemImageUrl) {
        Picasso.with(context)
                .load(itemImageUrl)
                .into(imageView);
    }
}
