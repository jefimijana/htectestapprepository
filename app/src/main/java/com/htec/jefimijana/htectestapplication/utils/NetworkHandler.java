package com.htec.jefimijana.htectestapplication.utils;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Used for all async web requests
 */
public class NetworkHandler {

    //No specific reason for using Volley. Just an example of third party library usage

    /**
     * Network handler
     */
    private static NetworkHandler mInstance;

    /**
     * Request que used for network requests
     */
    private RequestQueue mRequestQueue;

    /**
     * Current context
     */
    private static Context mContext;

    /**
     * Constructor
     *
     * @param context
     */
    public NetworkHandler(Context context) {

        //Initialize context
        mContext = context;

        //Instantiate the RequestQueue
        mRequestQueue = Volley.newRequestQueue(context);
    }

    /**
     * Gets instance of network handler
     *
     * @param context
     * @return mInstance instance of network handler
     */
    public static synchronized NetworkHandler getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new NetworkHandler(context);
        }
        return mInstance;
    }

    /**
     * Gets request que that holds all added requests
     *
     * @return mRequestQueue
     */
    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(mContext.getApplicationContext());
        }
        return mRequestQueue;
    }

    /**
     * Adds new request to request que
     *
     * @param req
     * @param <T>
     */
    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }
}
