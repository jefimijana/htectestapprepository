package com.htec.jefimijana.htectestapplication;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Data presenter test class
 */
@RunWith(MockitoJUnitRunner.class)
public class DataPresenterTest {

    @Mock
    private DataView view;
    @Mock
    private DataService service;
    private DataPresenter presenter;

    @Before
    public void setUp() throws Exception {
        presenter = new DataPresenter(view, service);
    }

    /**
     * Initial test for data fetching fail
     *
     * @throws Exception
     */
    @Test
    public void testDataDownloadFail() throws Exception {
        when(service.getData()).thenReturn(null);
        presenter.onDataRequested();
        verify(view).onDataRequestFail();
    }
}